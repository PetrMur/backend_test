from contextlib import asynccontextmanager
from functools import partial
import strawberry
from strawberry.types import Info
from fastapi import FastAPI
from strawberry.fastapi import BaseContext, GraphQLRouter
from databases import Database

from settings import Settings


class Context(BaseContext):
    db: Database

    def __init__(
        self,
        db: Database,
    ) -> None:
        self.db = db


@strawberry.type
class Author:
    name: str


@strawberry.type
class Book:
    title: str
    author: Author


@strawberry.type
class Query:

    @strawberry.field
    async def books(
        self,
        info: Info[Context, None],
        author_ids: list[int] | None = [],
        search: str | None = None,
        limit: int | None = None,
    ) -> list[Book]:

        # Надеюсь, я правильно понял задачу и Do NOT use dataloaders.
        # Если нет и елочку я писал зря, значит описание не совсем понятное
        # Я предположил, что нужен некий конструктор сырого sql запроса
        query = f"select books.title, authors.name from books join authors on authors.id = books.author_id"
        if author_ids or search:
            query = query + " where"
        if author_ids:
            author_ids = ', '.join([str(id_) for id_ in author_ids])
            query = query + f" author_id in ({author_ids})"
        if search:  # Что означает этот параметр не понятно. Я предположил, что поиск внутри поля
            if author_ids:
                query = query + f" and"
            query = query + f" title like '%{search}%'"
        if limit:
            query = query + f" limit {limit}"
        data = await info.context.db.fetch_all(query)
        data = [dict(book) for book in data]
        books = [Book(title=book.get('title'), author=Author(name=book.get('name'))) for book in data]
        return books


CONN_TEMPLATE = "postgresql+asyncpg://{user}:{password}@{host}:{port}/{name}"
settings = Settings()  # type: ignore
db = Database(
    CONN_TEMPLATE.format(
        user=settings.DB_USER,
        password=settings.DB_PASSWORD,
        port=settings.DB_PORT,
        host=settings.DB_SERVER,
        name=settings.DB_NAME,
    ),
)


@asynccontextmanager
async def lifespan(
    app: FastAPI,
    db: Database,
):
    async with db:
        yield
    await db.disconnect()

schema = strawberry.Schema(query=Query)
graphql_app = GraphQLRouter(  # type: ignore
    schema,
    context_getter=partial(Context, db),
)

app = FastAPI(lifespan=partial(lifespan, db=db))
app.include_router(graphql_app, prefix="/graphql")
